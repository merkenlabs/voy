package com.merkenlabs.voy

import com.jayway.jsonpath.JsonPath
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.reactive.server.WebTestClient

class FullApiOrderedTest : IT() {

    @Autowired
    lateinit var webTestClient: WebTestClient

    companion object {
        var groupId: String? = null
    }

    @Test
    @WithMockUser(authorities = ["SCOPE_USER"])
    fun `integration test`() {
        `a create a group and return it with id`()
        `b add participants to group`()
        `c check participants after adding`()
        `d remove participant`()
        `e check participants after removal`()
        `f remove group`()
        `g check removed group`()
    }

    @WithMockUser(authorities = ["SCOPE_USER"])
    fun `a create a group and return it with id`() {
        val jsonRequestBody = """{"name": "El Llano bestado"}"""

        val result = webTestClient.post().uri("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue((jsonRequestBody)).exchange() //
                .expectStatus().isOk //
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.status.code").isEqualTo(200)
                .jsonPath("$.status.message").isEqualTo("OK")
                .jsonPath("$.result.id").isNotEmpty
                .jsonPath("$.result.name").isEqualTo("El Llano bestado")
                .returnResult()

        val json = String(result.responseBody!!)

        groupId = JsonPath.read<String>(json, "$.result.id")

    }

    @WithMockUser(authorities = ["SCOPE_USER"])
    fun `b add participants to group`() {
        addParticipant("""{"username": "Vargas", "email": "jvar@test.com", "fullName": "Jaime Vargas"}""")
        addParticipant("""{"username": "Nicho", "email": "nicho@test.com", "fullName": "Cristian Lopez"}""")
        addParticipant("""{"username": "Nobody", "email": "nobo@test.com", "fullName": "Mr Nobody"}""")
    }

    private fun addParticipant(jsonRequestBody: String) {
        webTestClient.post().uri("/api/groups/{groupId}/participant", groupId)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue((jsonRequestBody)).exchange() //
                .expectStatus().isOk //
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.status.code").isEqualTo(200)
    }

    @WithMockUser(authorities = ["SCOPE_USER"])
    fun `c check participants after adding`() {
        val jsonResponse = """
{
  "status": {
    "code": 200,
    "message": "OK"
  },
  "result": {
    "id": "$groupId",
    "name": "El Llano bestado",
    "participants": [
      {
        "username": "Vargas",
        "email": "jvar@test.com",
        "fullName": "Jaime Vargas",
        "familyName": null,
        "givenName": null,
        "pictureUrl": null,
        "scopes": null,
        "id": "Vargas",
        "nickname": null
      },
      {
        "username": "Nicho",
        "email": "nicho@test.com",
        "fullName": "Cristian Lopez",
        "familyName": null,
        "givenName": null,
        "pictureUrl": null,
        "scopes": null,
        "id": "Nicho",
        "nickname": null
      },
      {
        "username": "Nobody",
        "email": "nobo@test.com",
        "fullName": "Mr Nobody",
        "familyName": null,
        "givenName": null,
        "pictureUrl": null,
        "scopes": null,
        "id": "Nobody",
        "nickname": null
      }
    ]
  }
}
        """

        getGroupOk(groupId!!, jsonResponse)
    }

    private fun getGroupOk(groupId: String, jsonResponse: String) {
        getGroup(groupId)
                .expectStatus().isOk
                .expectHeader().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)
                .expectBody()
                .json(jsonResponse)
    }

    private fun getGroup(groupId: String): WebTestClient.ResponseSpec {
        return webTestClient.get().uri("/api/groups/{groupId}", groupId)
                .exchange()
    }

    @WithMockUser(authorities = ["SCOPE_USER"])
    fun `d remove participant`() {
        val jsonRequestBody = """{"username": "Nobody", "email": "nobo@test.com", "fullName": "Mr Nobody"}"""

        webTestClient.put().uri("/api/groups/{groupId}/participant", groupId)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue((jsonRequestBody)).exchange() //
                .expectStatus().isOk //
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.status.code").isEqualTo(200)
    }

    @WithMockUser(authorities = ["SCOPE_USER"])
    fun `e check participants after removal`() {
        val jsonResponse = """
{
  "status": {
    "code": 200,
    "message": "OK"
  },
  "result": {
    "id": "$groupId",
    "name": "El Llano bestado",
    "participants": [
      {
        "username": "Vargas",
        "email": "jvar@test.com",
        "fullName": "Jaime Vargas",
        "familyName": null,
        "givenName": null,
        "pictureUrl": null,
        "scopes": null,
        "id": "Vargas",
        "nickname": null
      },
      {
        "username": "Nicho",
        "email": "nicho@test.com",
        "fullName": "Cristian Lopez",
        "familyName": null,
        "givenName": null,
        "pictureUrl": null,
        "scopes": null,
        "id": "Nicho",
        "nickname": null
      }
    ]
  }
}

        """

        getGroupOk(groupId!!, jsonResponse)
    }

    @WithMockUser(authorities = ["SCOPE_USER"])
    fun `f remove group`() {
        webTestClient.delete().uri("/api/groups/{groupId}", groupId).exchange()
                .expectStatus().isOk //
                .expectBody().isEmpty
    }

    @WithMockUser(authorities = ["SCOPE_USER"])
    fun `g check removed group`() {
        getGroup(groupId!!)
                .expectStatus().isOk
                .expectBody().isEmpty
    }

}