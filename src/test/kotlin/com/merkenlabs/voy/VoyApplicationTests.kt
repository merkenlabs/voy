package com.merkenlabs.voy

import com.merkenlabs.voy.api.handler.base.NewMatchRequest
import com.merkenlabs.voy.model.Court
import com.merkenlabs.voy.model.Match
import com.merkenlabs.voy.model.Player
import com.merkenlabs.voy.repository.CourtRepository
import com.merkenlabs.voy.repository.MatchRepository
import com.merkenlabs.voy.repository.PlayerRepository
import mu.KLogging
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.web.reactive.function.BodyInserters
import java.time.LocalDateTime

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class VoyApplicationTests : IT() {

    companion object : KLogging()

    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private lateinit var webTestClient: WebTestClient

    @Autowired
    private lateinit var courtRepository: CourtRepository

    @Autowired
    private lateinit var playerRepository: PlayerRepository

    @Autowired
    private lateinit var matchRepository: MatchRepository

    @BeforeAll
    fun before() {
        logger.info { "BeforeMark" }
        val court = courtRepository.save(Court("001", "Verde grande")).block()!!
        playerRepository.save(Player("juanito", "juanito@test.com", "Juanito Full").apply { nickname = "Juanito" }).block()
        matchRepository.save(Match(organizerId = "Varguitas", matchDate = LocalDateTime.now(), courtId = court.id!!, court = court)).block()
        matchRepository.save(Match(organizerId = "Otro", matchDate = LocalDateTime.now(), courtId = court.id!!, court = court)).block()
    }

    @Test
    @WithMockUser(authorities = ["SCOPE_USER"])
    fun `when Adding A Court with date and hour, Return the next match id`() {
        val jsonRequestBody = """
{
	"organizerId": "Varguitas",
	"matchDate": "2018-03-06 21:00",
	"courtId": "001"
}
"""

        webTestClient.post().uri("/api/pediLaCancha")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue((jsonRequestBody)).exchange() //
                .expectStatus().isOk //
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.status.code").isEqualTo(200)
                .jsonPath("$.status.message").isEqualTo("OK")
                .jsonPath("$.result.match.id").isNotEmpty
                .jsonPath("$.result.match.organizerId").isEqualTo("Varguitas")
                .jsonPath("$.result.match.matchDate").isEqualTo("2018-03-06 21:00")
                .jsonPath("$.result.match.court.name").isEqualTo("Verde grande")
                .jsonPath("$.result.match.players").isArray
                .jsonPath("$.result.match.players").isEmpty
    }

    @Test
    @WithMockUser(authorities = ["SCOPE_USER"])
    fun `when saying I'm going, and there is a match created, return OK`() {
        webTestClient.post().uri("/api/voy/{nickname}", "Juanito")
                .contentType(MediaType.APPLICATION_JSON)
                .exchange() //
                .expectStatus().isOk //
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.status.code").isEqualTo(200)
                .jsonPath("$.status.message").isEqualTo("OK")
    }

    @Test
    @WithMockUser(authorities = ["SCOPE_USER"])
    fun `when asking for current match, return the most new match created`() {
        webTestClient.get().uri("/api/partidoActual")
                .exchange() //
                .expectStatus().isOk //
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.status.code").isEqualTo(200)
                .jsonPath("$.status.message").isEqualTo("OK")

    }

    @Test
    @WithMockUser(authorities = ["SCOPE_USER", "USER", "ROLE_USER"])
    fun `when adding a Group, Return the group with created id`() {
        val jsonRequestBody = """
{
	"name": "El Llano bestado"
}
"""

        webTestClient.post().uri("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue((jsonRequestBody)).exchange() //
                .expectStatus().isOk //
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.status.code").isEqualTo(200)
                .jsonPath("$.status.message").isEqualTo("OK")
                .jsonPath("$.result.id").isNotEmpty
                .jsonPath("$.result.name").isEqualTo("El Llano bestado")

    }

    @Test
    @WithMockUser(authorities = ["SCOPE_USER"])
    fun `When adding a match with Court, the court must be saved`() {

        val newMatchRequest = NewMatchRequest(courtId = "001", organizerId = "varguitas", matchDate = LocalDateTime.now())

        webTestClient.post().uri("/api/matches")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(newMatchRequest)).exchange()
                .expectStatus().isOk
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.status.code").isEqualTo(200)
                .jsonPath("$.status.message").isEqualTo("OK")
                .jsonPath("$.result.match.id").isNotEmpty
                .jsonPath("$.result.match.court.name").isEqualTo("Verde grande")
    }

    @Test
    @WithMockUser(authorities = ["SCOPE_USER"])
    fun `When adding a match with Court that doesn't exist, it must reply error`() {
        val newMatchRequest = NewMatchRequest(courtId = "004", organizerId = "varguitas", matchDate = LocalDateTime.now())

        webTestClient.post().uri("/api/matches")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(newMatchRequest)).exchange()
                .expectStatus().is4xxClientError
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.status.code").isEqualTo(400)
                .jsonPath("$.result.code").isEqualTo("COURT_NOT_FOUND")
    }

    @Test
    @WithMockUser(authorities = ["SCOPE_USER"])
    fun `check basic cors configuration`() {
        webTestClient.get().uri("/")
                .header("Origin", "http://anothersite.org")
                .header("Access-Control-Request-Method", "GET")
                .exchange()
                .expectStatus().isOk
    }

    @Test
    @WithMockUser(authorities = ["SCOPE_USER"])
    fun `check delete cors configuration`() {
        webTestClient.delete().uri("/api/groups/{groupId}", "FAKEID")
                .header("Origin", "http://anothersite.org")
                .header("Access-Control-Request-Method", "DELETE")
                .exchange()
                .expectStatus().isOk
    }

    @Test
    @WithMockUser(authorities = ["SCOPE_USER"], username = "Varguitas")
    fun `When asking for matches, it must return matches where I'm organizer or player`() {
        val matchesPath = "$"


        webTestClient.get().uri("/api/matches")
                .exchange()
                .expectStatus().isOk //
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath(matchesPath).isArray
                .jsonPath(matchesPath).isNotEmpty
                .jsonPath("$matchesPath[0].organizerId").isEqualTo("Varguitas")
                .jsonPath("$[?(@.organizerId == 'Varguitas' || @.players[*].username == 'Varguitas')]").isNotEmpty

    }

}
