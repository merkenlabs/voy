package com.merkenlabs.voy

import com.merkenlabs.voy.api.controller.IndexController
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.getForEntity
import org.springframework.security.test.context.support.WithMockUser

class WebApplicationTests : IT() {

    @Autowired
    lateinit var indexController: IndexController

    @Autowired
    lateinit var restTemplate: TestRestTemplate

    @Test
    fun `context load`() {
        assertThat(indexController)::isNotNull
    }

    @Test
    @WithMockUser(authorities = ["SCOPE_USER"])
    fun `when requesting index page, return page with body containing Hola!`() {
        val stringToBeFound = "index"
        assertThat(restTemplate.getForEntity<String>("/").body).contains(stringToBeFound)
    }

}
