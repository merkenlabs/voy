package com.merkenlabs.voy.security

import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier
import com.google.api.client.http.apache.v2.ApacheHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.merkenlabs.voy.config.properties.SecurityProperties
import com.merkenlabs.voy.security.google.LocalGoogleIDTokenVerifier
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.openssl.PEMKeyPair
import org.bouncycastle.openssl.PEMParser
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter
import org.springframework.boot.autoconfigure.security.reactive.PathRequest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames
import org.springframework.security.oauth2.jwt.NimbusReactiveJwtDecoder
import org.springframework.security.oauth2.jwt.ReactiveJwtDecoder
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.web.reactive.config.WebFluxConfigurer
import java.security.KeyPair
import java.security.Security
import java.security.interfaces.RSAPublicKey
import java.util.*

@Configuration
@EnableWebFluxSecurity
class SecurityConfig : WebFluxConfigurer {

    @Bean
    fun genKeys(securityProperties: SecurityProperties): KeyPair {
        Security.addProvider(BouncyCastleProvider())

        val fileResource = securityProperties.pemFile

        val pemParser = PEMParser(fileResource.inputStream.reader())
        val pemKeyPair = pemParser.readObject() as PEMKeyPair
        pemParser.close()

        val converter = JcaPEMKeyConverter()
        return converter.getKeyPair(pemKeyPair)
    }

    @Bean
    fun reactiveJwtDecoder(genKeys: KeyPair): ReactiveJwtDecoder {
        val publicKey = genKeys.public as RSAPublicKey
        return NimbusReactiveJwtDecoder(publicKey)
    }

    @Bean
    fun springSecurityFilterChain(
            http: ServerHttpSecurity,
            genKeys: KeyPair,
            reactiveJwtDecoder: ReactiveJwtDecoder
    ): SecurityWebFilterChain {
        http
                .authorizeExchange()
                .matchers(PathRequest.toStaticResources().atCommonLocations()).permitAll()
                .pathMatchers("/signin/*").permitAll()
                .pathMatchers("/api/**").hasAnyAuthority("SCOPE_USER", "SCOPE_ADMIN")
                .pathMatchers("/groups").hasRole("ADMIN")
                .pathMatchers("/").authenticated()
                .and().oauth2Login()
                .and()
                .csrf().disable()
                .oauth2ResourceServer()
                .jwt()
                .publicKey(genKeys.public as RSAPublicKey).jwtDecoder(reactiveJwtDecoder)
        return http.build()
    }

    @Bean
    fun idTokenVerifier(): GoogleIdTokenVerifier {
        LocalGoogleIDTokenVerifier.logger.info { "ClientID: ${OAuth2ParameterNames.CLIENT_ID}" }
        return GoogleIdTokenVerifier.Builder(ApacheHttpTransport(), JacksonFactory())
                .setAudience(Collections.singletonList(OAuth2ParameterNames.CLIENT_ID))
                .build()
    }

}
