package com.merkenlabs.voy.security

import org.springframework.security.core.userdetails.MapReactiveUserDetailsService
import org.springframework.security.core.userdetails.ReactiveUserDetailsService
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono

@Component
class UserDetailsService : ReactiveUserDetailsService {

    final val user: UserDetails = User.withDefaultPasswordEncoder()
            .username("user")
            .password("user")
            .roles("USER")
            .build()
    final val admin: UserDetails = User.withDefaultPasswordEncoder()
            .username("admin")
            .password("admin")
            .roles("ADMIN")
            .build()
    final val lecaros: UserDetails = User.withDefaultPasswordEncoder()
            .username("111858990053902927885")
            .password("sdhfkjsahflasglfas")
            .roles("USER", "ADMIN")
            .build()

    val service = MapReactiveUserDetailsService(user, admin, lecaros)

    override fun findByUsername(username: String?): Mono<UserDetails> {
        return service.findByUsername(username)
    }

}
