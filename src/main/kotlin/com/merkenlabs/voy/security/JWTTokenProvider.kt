package com.merkenlabs.voy.security

import com.nimbusds.jose.JWSAlgorithm
import com.nimbusds.jose.JWSHeader
import com.nimbusds.jose.JWSObject
import com.nimbusds.jose.Payload
import com.nimbusds.jose.crypto.RSASSASigner
import com.nimbusds.jwt.JWTClaimsSet
import mu.KLogging
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.security.KeyPair
import java.security.interfaces.RSAPrivateKey
import java.util.*

@Component
class JWTTokenProvider(
        private val keyPair: KeyPair
) {

    companion object : KLogging() {
        const val THREE_HOURS_IN_MILLIS = 1000 * 60 * 60 * 3L
    }

    @Value("\${security.jwt.token.validity-in-milliseconds}")
    var validityInMilliseconds: Long = THREE_HOURS_IN_MILLIS

    fun createToken(username: String, roles: List<String>): String {
        logger.info("Creating token for user $username($roles)")
        logger.debug("will expire in $validityInMilliseconds")
        val claimsSet = JWTClaimsSet.Builder()
                .subject(username)
                .expirationTime(Date(Date().time + validityInMilliseconds))
                .claim("scp", roles)
                .build()

        val privateKey = keyPair.private as RSAPrivateKey

        val jwsObject = JWSObject(JWSHeader(JWSAlgorithm.RS256), Payload(claimsSet.toJSONObject()))
        jwsObject.sign(RSASSASigner(privateKey))

        return jwsObject.serialize()
    }

}
