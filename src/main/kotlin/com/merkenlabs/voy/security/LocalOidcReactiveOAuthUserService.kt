package com.merkenlabs.voy.security

import mu.KLogging
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.oauth2.client.oidc.userinfo.OidcReactiveOAuth2UserService
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest
import org.springframework.security.oauth2.client.userinfo.ReactiveOAuth2UserService
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser
import org.springframework.security.oauth2.core.oidc.user.OidcUser
import org.springframework.security.oauth2.core.oidc.user.OidcUserAuthority
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.cast
import java.util.stream.Collectors

@Component
class LocalOidcReactiveOAuthUserService(
        private val userService: UserDetailsService
) : ReactiveOAuth2UserService<OidcUserRequest, OidcUser> {
    private val delegate = OidcReactiveOAuth2UserService()

    companion object : KLogging()

    override fun loadUser(userRequest: OidcUserRequest?): Mono<OidcUser> {
        /* look for user information from reactive user service.
        If user doesn't exist it should be added directly.
        Email is already confirmed by login.
        ROLE_USER is added by default.
         */
        return delegate.loadUser(userRequest)
                .cast<DefaultOidcUser>()
                .flatMap {
                    logger.info { "name: ${it.name}" }
                    userService.findByUsername(it.name)
                }
                .map(UserDetails::getAuthorities)
                .flatMapIterable {
                    it
                }
                .map { authority ->
                    OidcUserAuthority(authority.authority, userRequest?.idToken, null)
                }
                .collect(Collectors.toSet())
                .map {
                    DefaultOidcUser(it, userRequest?.idToken)
                }
                .map {
                    logger.info { it }
                    it
                }
    }

}
