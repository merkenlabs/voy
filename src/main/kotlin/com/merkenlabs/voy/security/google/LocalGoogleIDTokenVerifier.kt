package com.merkenlabs.voy.security.google

import mu.KLogging
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.core.publisher.Mono


@Component
class LocalGoogleIDTokenVerifier {

    private val webClient = WebClient.create("https://oauth2.googleapis.com")

    companion object : KLogging()

    fun verify(idTokenString: String): Mono<TokenVerifyResponse> {
        logger.info { "Verify" }

        return webClient.get()
                .uri { uriBuilder ->
                    uriBuilder.path("/tokeninfo")
                            .queryParam("id_token", idTokenString).build()
                }
                .retrieve()
                .bodyToMono<TokenVerifyResponse>()
                .map {
                    logger.info { "This is the response $it" }
                    it
                }
    }

}
