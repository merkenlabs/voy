package com.merkenlabs.voy.security.google

data class TokenVerifyResponse(
        val sub: String, val email: String,
        val email_verified: String,
        val name: String,
        val picture: String?,
        val given_name: String,
        val family_name: String,
        val locale: String
)
