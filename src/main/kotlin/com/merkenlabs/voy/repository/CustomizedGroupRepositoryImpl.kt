package com.merkenlabs.voy.repository

import com.merkenlabs.voy.model.Group
import com.merkenlabs.voy.model.Player
import org.springframework.data.mongodb.core.ReactiveMongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.data.mongodb.core.updateFirst
import reactor.core.publisher.Mono

class CustomizedGroupRepositoryImpl(
        private val mongoTemplate: ReactiveMongoOperations
) : CustomizedGroupRepository {

    override fun addParticipant(groupId: String, participant: Player): Mono<Long> {

        return mongoTemplate.updateFirst<Group>(
                whereGroupIdIs(groupId),
                Update().push("participants", participant)
        ).map { it.matchedCount }

    }

    override fun removeParticipant(groupId: String, participant: Player): Mono<Long> {

        return mongoTemplate.updateFirst<Group>(
                whereGroupIdIs(groupId),
                Update().pull("participants", participant)
        ).map { it.modifiedCount }

    }

    private fun whereGroupIdIs(groupId: String) =
            Query.query(Criteria.where("id").`is`(groupId))

}
