package com.merkenlabs.voy.repository

import com.merkenlabs.voy.model.Match
import mu.KLogging
import org.springframework.data.mongodb.core.ReactiveMongoOperations
import org.springframework.data.mongodb.core.findById
import reactor.core.publisher.Mono

class CustomizedMatchRepositoryImpl(
        private val courtRepository: CourtRepository,
        private val mongoTemplate: ReactiveMongoOperations
) : CustomizedMatchRepository {

    companion object : KLogging()

    override fun findById(matchId: String): Mono<Match> {
        logger.info { "FindByID custom" }
        return mongoTemplate.findById<Match>(matchId)
                .map(::findCourt)
                .flatMap {
                    it
                }
    }

    private fun findCourt(match: Match): Mono<Match> {
        logger.info { "FindCourt custom" }
        if (match.courtId == null) {
            return Mono.just(match)
        }
        return courtRepository.findById(match.courtId)
                .map {
                    match.copy(court = it)
                }
    }

}
