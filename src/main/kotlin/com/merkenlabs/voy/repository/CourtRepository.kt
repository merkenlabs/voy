package com.merkenlabs.voy.repository

import com.merkenlabs.voy.model.Court
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import reactor.core.publisher.Mono

interface CourtRepository : ReactiveMongoRepository<Court, String> {

    fun findByName(name: String): Mono<Court>

}
