package com.merkenlabs.voy.repository

import com.merkenlabs.voy.model.Match
import org.springframework.data.mongodb.repository.ReactiveMongoRepository

interface MatchRepository : ReactiveMongoRepository<Match, String>, CustomizedMatchRepository
