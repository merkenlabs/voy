package com.merkenlabs.voy.repository

import com.merkenlabs.voy.model.Player
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import reactor.core.publisher.Mono

interface PlayerRepository : ReactiveMongoRepository<Player, String> {

    fun findByNickname(nickname: String): Mono<Player>

}
