package com.merkenlabs.voy.repository

import com.merkenlabs.voy.model.Group
import org.springframework.data.repository.reactive.ReactiveCrudRepository

interface GroupRepository : ReactiveCrudRepository<Group, String>, CustomizedGroupRepository
