package com.merkenlabs.voy.repository

import com.merkenlabs.voy.model.Player
import reactor.core.publisher.Mono

interface CustomizedGroupRepository {

    fun addParticipant(groupId: String, participant: Player): Mono<Long>

    fun removeParticipant(groupId: String, participant: Player): Mono<Long>

}
