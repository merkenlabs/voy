package com.merkenlabs.voy.repository

import com.merkenlabs.voy.model.Match
import reactor.core.publisher.Mono

interface CustomizedMatchRepository {

    fun findById(matchId: String): Mono<Match>

}
