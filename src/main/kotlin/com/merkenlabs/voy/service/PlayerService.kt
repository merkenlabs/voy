package com.merkenlabs.voy.service

import com.merkenlabs.voy.model.Player
import com.merkenlabs.voy.repository.PlayerRepository
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class PlayerService(
        private val playerRepository: PlayerRepository
) {

    fun save(player: Player): Mono<Player> {
        return playerRepository.save(player)
    }

}
