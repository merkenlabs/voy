package com.merkenlabs.voy.service

import com.merkenlabs.voy.model.Court
import com.merkenlabs.voy.repository.CourtRepository
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono

@Component
class CourtService(private val courtRepository: CourtRepository) {

    fun findById(courtId: String): Mono<Court> {
        return courtRepository.findById(courtId)
    }

}
