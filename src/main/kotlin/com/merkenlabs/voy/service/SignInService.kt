package com.merkenlabs.voy.service

import com.merkenlabs.voy.api.handler.base.SignInRequest
import com.merkenlabs.voy.api.handler.base.SignInResponse
import com.merkenlabs.voy.model.Player
import com.merkenlabs.voy.security.JWTTokenProvider
import com.merkenlabs.voy.security.google.LocalGoogleIDTokenVerifier
import com.merkenlabs.voy.security.google.TokenVerifyResponse
import mu.KLogging
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class SignInService(
        private val idTokenVerifierLocal: LocalGoogleIDTokenVerifier,
        private val playerService: PlayerService,
        private val jwtTokenProvider: JWTTokenProvider
) {

    companion object : KLogging()

    fun signInUser(signInRequest: SignInRequest): Mono<SignInResponse> {
        logger.info { "signInUser call" }
        return idTokenVerifierLocal.verify(signInRequest.idToken)
                .map(::tokenVerifyResponseToPlayer)
                .map(::addRoles)
                .map(::createUser)
                .flatMap(::genAndReturn)
    }

    private fun tokenVerifyResponseToPlayer(tokenVerifyResponse: TokenVerifyResponse): Player {
        return Player(
                tokenVerifyResponse.sub, tokenVerifyResponse.email,
                tokenVerifyResponse.name, tokenVerifyResponse.family_name,
                tokenVerifyResponse.given_name, tokenVerifyResponse.picture)
    }

    private fun generateToken(player: Player): String {
        return jwtTokenProvider.createToken(player.username, player.scopes!!.split(","))
    }

    private fun genAndReturn(player: Mono<Player>): Mono<SignInResponse> {

        return player.map {
            val token = generateToken(it)
            SignInResponse(token, it)
        }
    }

    private fun createUser(player: Player): Mono<Player> {
        logger.info { "Creating user: $player" }
        return playerService.save(player)
    }

    private fun addRoles(player: Player): Player {
        return player.copy(scopes = "USER")
    }

}
