package com.merkenlabs.voy.service

import com.merkenlabs.voy.api.handler.base.ApiResponse
import com.merkenlabs.voy.api.handler.base.ApiStatus
import com.merkenlabs.voy.api.handler.base.NewGroupRequest
import com.merkenlabs.voy.api.handler.base.NewGroupResponse
import com.merkenlabs.voy.model.Group
import com.merkenlabs.voy.repository.GroupRepository
import mu.KLogging
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class GroupService(val groupRepository: GroupRepository) {

    companion object : KLogging()


    fun add(newGroupRequest: NewGroupRequest): Mono<ApiResponse> {
        logger.info { "request=$newGroupRequest" }
        logger.info { "saving group: $newGroupRequest" }

        val insertedGroupMono = groupRepository.save(Group(name = newGroupRequest.name))


        return insertedGroupMono.map {
            val response = ApiResponse(ApiStatus(), NewGroupResponse(it))

            logger.info { "response=$response" }

            return@map response
        }
    }

    fun listAll(): Flux<NewGroupResponse> {
        logger.info { "groups.listAll()" }

        val allGroups = groupRepository.findAll()
        logger.info { "groups count: ${allGroups.count()}" }

        return allGroups.map {
            val response = NewGroupResponse(it)

            logger.info { "response=$response" }

            return@map response
        }
    }
}
