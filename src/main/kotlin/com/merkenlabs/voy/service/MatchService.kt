package com.merkenlabs.voy.service

import com.merkenlabs.voy.api.handler.base.*
import com.merkenlabs.voy.exception.CourtNotFoundException
import com.merkenlabs.voy.model.Match
import com.merkenlabs.voy.model.Player
import com.merkenlabs.voy.repository.MatchRepository
import com.merkenlabs.voy.repository.PlayerRepository
import mu.KLogging
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.security.Principal

@Service
class MatchService(
        private val matchRepository: MatchRepository,
        private val playerRepository: PlayerRepository,
        private val courtService: CourtService
) {

    companion object : KLogging()

    fun add(newMatchRequest: NewMatchRequest): Mono<ApiResponse> {

        return courtService.findById(newMatchRequest.courtId)
                .switchIfEmpty(Mono.error(CourtNotFoundException(message = "Court Not Found")))
                .flatMap {
                    matchRepository.save(Match(organizerId = newMatchRequest.organizerId,
                            matchDate = newMatchRequest.matchDate, courtId = it.id, court = it))
                }.map { match ->
                    val response = ApiResponse(ApiStatus(), NewMatchResponse(match))
                    logger.info { "response=$response" }
                    response
                }
    }

    fun addPlayerByNickname(nickname: String, voyRequest: VoyRequest?): Mono<ApiResponse> =
            playerRepository.findByNickname(nickname)
                    .flatMap { addPlayer(it) }

    private fun addPlayer(player: Player): Mono<ApiResponse> {

        logger.info { "player=$player" }

        return currentMatch()
                .flatMap {
                    val matchForUpdate = it.copy(players = it.players.toMutableList().apply { add(player) })
                    matchRepository.save(matchForUpdate)
                }
                .map {
                    val response = ApiResponse(ApiStatus(), "OK")

                    logger.info { "response=$response" }

                    response
                }
    }

    fun getCurrentMatch(): Mono<ApiResponse> =
            currentMatch()
                    .map {
                        ApiResponse(ApiStatus(), it)
                    }

    private fun currentMatch(): Mono<Match> =
            matchRepository.findAll()
                    .last()

    fun findAll(principal: Principal): Flux<Match> {
        return matchRepository.findAll().filter { match ->
            match.organizerId == principal.name || match.players.any {
                it.username == principal.name
            }
        }
    }

}
