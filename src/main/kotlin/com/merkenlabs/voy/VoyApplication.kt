package com.merkenlabs.voy

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication
@ConfigurationPropertiesScan
class VoyApplication

fun main(args: Array<String>) {
    runApplication<VoyApplication>(*args)
}
