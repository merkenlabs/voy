package com.merkenlabs.voy.exception

class CourtNotFoundException (message: String): VoyException("COURT_NOT_FOUND", message)