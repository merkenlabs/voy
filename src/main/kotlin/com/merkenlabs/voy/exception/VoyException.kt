package com.merkenlabs.voy.exception

open class VoyException(val code: String, message: String): Throwable(message)