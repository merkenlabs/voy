package com.merkenlabs.voy.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "courts")
data class Court(
        @Id var id: String? = null,
        val name: String
        )
