package com.merkenlabs.voy.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "groups")
data class Group(
        @Id var id: String? = null,
        var name: String,
        var participants: MutableList<Player> = mutableListOf()
)
