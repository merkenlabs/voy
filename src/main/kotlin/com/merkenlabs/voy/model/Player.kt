package com.merkenlabs.voy.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "players")
data class Player(
        @Indexed(unique = true)
        val username: String,
        val email: String,
        val fullName: String,
        val familyName: String? = null,
        val givenName: String? = null,
        val pictureUrl: String? = null,
        var scopes: String? = null
) {
    @Id
    var id: String? = username
    var nickname: String? = null
}
