package com.merkenlabs.voy.model

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonFormat.Shape
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

@Document(collection = "matches")
data class Match(
        @Id var id: String? = null,
        val organizerId: String,
        @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd HH:mm") val matchDate: LocalDateTime,
        val courtId: String?,
        var court: Court?,
        val players: List<Player> = ArrayList()
)
