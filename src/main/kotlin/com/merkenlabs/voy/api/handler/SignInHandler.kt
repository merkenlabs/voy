package com.merkenlabs.voy.api.handler

import com.merkenlabs.voy.api.handler.base.ApiResponse
import com.merkenlabs.voy.api.handler.base.ApiStatus
import com.merkenlabs.voy.api.handler.base.SignInRequest
import com.merkenlabs.voy.service.SignInService
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.bodyToMono
import reactor.core.publisher.Mono

@Component
class SignInHandler(private val signInService: SignInService) : HandlerBase() {

    fun google(req: ServerRequest): Mono<ServerResponse> =
            req.bodyToMono<SignInRequest>()
                    .flatMap { signInRequest ->
                        signInService.signInUser(signInRequest)
                    }
                    .flatMap { okResponse(ApiResponse(ApiStatus(), it)) }

}
