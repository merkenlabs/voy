package com.merkenlabs.voy.api.handler

import com.merkenlabs.voy.api.handler.base.ApiResponse
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.body
import reactor.core.publisher.Mono

abstract class HandlerBase {

    protected fun okResponse(apiResponse: ApiResponse): Mono<ServerResponse> =
            ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
                    .bodyValue(apiResponse)

    protected fun badRequestResponse(apiResponse: ApiResponse): Mono<ServerResponse> =
            ServerResponse.badRequest()
                    .contentType(MediaType.APPLICATION_JSON)
                    .bodyValue(apiResponse)

}
