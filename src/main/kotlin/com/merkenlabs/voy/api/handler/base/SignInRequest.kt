package com.merkenlabs.voy.api.handler.base

data class SignInRequest(val idToken: String)
