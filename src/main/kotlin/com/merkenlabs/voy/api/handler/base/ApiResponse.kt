package com.merkenlabs.voy.api.handler.base

data class ApiResponse(
        val status: ApiStatus,
        val result: Any? = null
)
