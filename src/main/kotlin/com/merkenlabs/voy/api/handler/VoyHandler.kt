package com.merkenlabs.voy.api.handler

import com.merkenlabs.voy.api.handler.base.ApiResponse
import com.merkenlabs.voy.api.handler.base.ApiStatus
import com.merkenlabs.voy.api.handler.base.NewMatchRequest
import com.merkenlabs.voy.api.handler.base.VoyRequest
import com.merkenlabs.voy.service.MatchService
import mu.KLogging
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.bodyToMono
import reactor.core.publisher.Mono

@Component
class VoyHandler(val matchService: MatchService) : HandlerBase() {

    companion object : KLogging()

    fun pedirLaCancha(req: ServerRequest): Mono<ServerResponse> =
            req.bodyToMono<NewMatchRequest>()
                    .log()
                    .flatMap {
                        matchService.add(it)
                    }
                    .flatMap {
                        logger.info { "Response inside: $it" }
                        okResponse(it)
                    }
                    .onErrorResume {
                        val response = ApiResponse(ApiStatus(HttpStatus.BAD_REQUEST.value(),it.localizedMessage), it)
                        badRequestResponse(response)
                    }


    fun voy(req: ServerRequest): Mono<ServerResponse> {

        val nickname = req.pathVariable("nickname")

        return req
                .bodyToMono<VoyRequest>()
                .flatMap { addPlayer(nickname, it) }
                .switchIfEmpty(addPlayer(nickname))

    }

    private fun addPlayer(nickname: String, voyRequest: VoyRequest? = null): Mono<ServerResponse> {
        return matchService.addPlayerByNickname(nickname, voyRequest)
                .flatMap(::okResponse)
    }

    fun partidoActual(req: ServerRequest): Mono<ServerResponse> {
        return matchService.getCurrentMatch()
                .flatMap(::okResponse)
    }

}
