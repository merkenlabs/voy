package com.merkenlabs.voy.api.handler.base

import com.merkenlabs.voy.model.Group

data class NewGroupResponse(
        val group: Group? = null
)
