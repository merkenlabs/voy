package com.merkenlabs.voy.api.handler.base

data class ApiStatus(
        val code: Int = 200,
        val message: String = "OK"
)
