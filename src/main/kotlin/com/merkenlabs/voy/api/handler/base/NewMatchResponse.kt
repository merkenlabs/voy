package com.merkenlabs.voy.api.handler.base

import com.merkenlabs.voy.model.Match

data class NewMatchResponse(
        val match: Match? = null
)
