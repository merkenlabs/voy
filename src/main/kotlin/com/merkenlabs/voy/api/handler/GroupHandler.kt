package com.merkenlabs.voy.api.handler

import com.merkenlabs.voy.api.handler.base.ApiResponse
import com.merkenlabs.voy.api.handler.base.ApiStatus
import com.merkenlabs.voy.model.Group
import com.merkenlabs.voy.model.Player
import com.merkenlabs.voy.repository.GroupRepository
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.bodyToMono
import reactor.core.publisher.Mono

@Component
class GroupHandler(val groupRepository: GroupRepository) : HandlerBase() {

    fun createGroup(req: ServerRequest): Mono<ServerResponse> {
        return req.bodyToMono<Group>()
                .flatMap { groupRepository.save(it) }
                .map { ApiResponse(ApiStatus(), it) }
                .flatMap { okResponse(it) }
    }

    fun getGroup(req: ServerRequest): Mono<ServerResponse> {
        val groupId = getGroupId(req)

        return groupRepository.findById(groupId)
                .map { ApiResponse(ApiStatus(), it) }
                .flatMap { okResponse(it) }
    }

    fun deleteGroup(req: ServerRequest): Mono<ServerResponse> {
        val groupId = getGroupId(req)

        return groupRepository.deleteById(groupId)
                .map { ApiResponse(ApiStatus(), it) }
                .flatMap { okResponse(it) }
    }

    fun addParticipant(req: ServerRequest): Mono<ServerResponse> {
        val groupId = getGroupId(req)
        val participantToAdd = req.bodyToMono<Player>()

        val addedParticipant = participantToAdd.flatMap { participant ->
            groupRepository.addParticipant(groupId, participant)
        }

        return addedParticipant
                .map { ApiResponse(ApiStatus(), it) }
                .flatMap { okResponse(it) }
    }

    fun removeParticipant(req: ServerRequest): Mono<ServerResponse> {
        val groupId = getGroupId(req)
        val participantToRemove = req.bodyToMono<Player>()

        val removedParticipant = participantToRemove.flatMap { participant ->
            groupRepository.removeParticipant(groupId, participant)
        }

        return removedParticipant
                .map { ApiResponse(ApiStatus(), it) }
                .flatMap { okResponse(it) }
    }

    private fun getGroupId(req: ServerRequest): String = req.pathVariable("groupId")

    fun listAll(req: ServerRequest): Mono<ServerResponse> {
        return ServerResponse.ok().body(groupRepository.findAll(), Group::class.java)
    }
}
