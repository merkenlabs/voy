package com.merkenlabs.voy.api.handler

import com.merkenlabs.voy.api.handler.base.ApiResponse
import com.merkenlabs.voy.api.handler.base.ApiStatus
import com.merkenlabs.voy.api.handler.base.NewMatchRequest
import com.merkenlabs.voy.model.Match
import com.merkenlabs.voy.service.MatchService
import mu.KLogging
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.bodyToMono
import reactor.core.publisher.Mono

@Component
class MatchHandler(
        val matchService: MatchService
) : HandlerBase() {

    companion object : KLogging()

    fun addMatch(request: ServerRequest): Mono<ServerResponse> =
            request.bodyToMono<NewMatchRequest>()
                    .flatMap { matchService.add(it) }
                    .flatMap { okResponse(it) }
                    .onErrorResume {
                        badRequestResponse(ApiResponse(ApiStatus(HttpStatus.BAD_REQUEST.value(), it.localizedMessage), it))
                    }


    fun listAll(request: ServerRequest): Mono<ServerResponse> {
        return request.principal()
                .map {
                    ServerResponse.ok().body(matchService.findAll(it), Match::class.java)
                }.flatMap {
                    it
                }
    }
}
