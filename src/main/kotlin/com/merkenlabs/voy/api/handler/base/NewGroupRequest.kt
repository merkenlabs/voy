package com.merkenlabs.voy.api.handler.base

data class NewGroupRequest (val name:String)
