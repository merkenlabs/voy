package com.merkenlabs.voy.api.handler.base


data class VoyRequest(
        val invitedQuantity: Int
)
