package com.merkenlabs.voy.api.handler.base

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonFormat.Shape
import java.time.LocalDateTime

data class NewMatchRequest(
        val organizerId: String,
        @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd HH:mm") val matchDate: LocalDateTime,
        val courtId: String
)
