package com.merkenlabs.voy.api.handler.base

import com.merkenlabs.voy.model.Player

data class SignInResponse(
        val accessToken: String,
        val player: Player
)
