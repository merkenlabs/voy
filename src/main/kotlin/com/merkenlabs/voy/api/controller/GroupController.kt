package com.merkenlabs.voy.api.controller

import com.merkenlabs.voy.api.handler.base.NewGroupRequest
import com.merkenlabs.voy.model.Group
import com.merkenlabs.voy.service.GroupService
import mu.KLogging
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class GroupController(
        private val groupService: GroupService
) {

    companion object : KLogging()

    @GetMapping("/groups")
    fun listGroups(model: Model): String {
        val groupsStream = groupService.listAll()
        model.addAttribute("groupsList", groupsStream)
        model.addAttribute("group", Group("", ""))

        return "groups"
    }

    @PostMapping("/groups")
    fun saveGroup(model: Model, @ModelAttribute group: Group): String {
        groupService.add(NewGroupRequest(group.name)).subscribe()
        val groups = groupService.listAll()
        model.addAttribute("groupsList", groups)
        return "groups"
    }

}
