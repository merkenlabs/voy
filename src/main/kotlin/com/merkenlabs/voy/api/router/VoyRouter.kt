package com.merkenlabs.voy.api.router

import com.merkenlabs.voy.api.handler.GroupHandler
import com.merkenlabs.voy.api.handler.MatchHandler
import com.merkenlabs.voy.api.handler.SignInHandler
import com.merkenlabs.voy.api.handler.VoyHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.router

@Configuration
class VoyRouter(private val voyHandler: VoyHandler, private val groupHandler: GroupHandler,
                private val signInHandler: SignInHandler, private val matchHandler: MatchHandler) {

    @Bean
    fun apis() = router {
        (accept(MediaType.APPLICATION_JSON) and "/api") //
                .nest {
                    POST("/pediLaCancha", voyHandler::pedirLaCancha)
                    GET("/partidoActual", voyHandler::partidoActual)
                    POST("/voy/{nickname}", voyHandler::voy)
                    GET("/groups", groupHandler::listAll)
                    POST("/groups", groupHandler::createGroup)
                    GET("/groups/{groupId}", groupHandler::getGroup)
                    DELETE("/groups/{groupId}", groupHandler::deleteGroup)
                    POST("/groups/{groupId}/participant", groupHandler::addParticipant)
                    PUT("/groups/{groupId}/participant", groupHandler::removeParticipant)
                    POST("/matches", matchHandler::addMatch)
                    GET("/matches", matchHandler::listAll)
                }
        (accept(MediaType.APPLICATION_JSON) and "/")
                .nest {
                    POST("signin/google", signInHandler::google)
                }
    }
}
