package com.merkenlabs.voy.config

import com.merkenlabs.voy.repository.MatchRepository
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories

@Configuration
@EnableReactiveMongoRepositories(basePackageClasses = [MatchRepository::class])
class MongoDbConfig
