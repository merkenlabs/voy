package com.merkenlabs.voy.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.core.io.Resource

@ConfigurationProperties("app.security")
@ConstructorBinding
data class SecurityProperties(
        val pemFile: Resource
)
