# voy-api

## Dev environment

* docker & docker-compose
* Java JDK 8

## How to run

### For local development

* `docker-compose up  -d`
* Start the app from your IDE or from Gradle with `./gradlew bootRun`

## Docker

### Build

`./gradlew docker`

### Run

`./gradlew dockerRun`

## Release notes

### 0.1.0

- Add Google OAuth2 authentication support.
- Add JWT token generation and validation.
- Add business exceptions. This provides further information on error when creating a match.

## How to connect to the API

### Authenticate and authorize with Google

1. The client(mobile, web, etc.) must request an OAuth2 token from Google. See [this guide](https://developers.google.com/identity/sign-in/android/backend-auth).
1. The client must send the id_token to "/signin/google".
1. The server will validate the token, register the user and provide an access token.