import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.bundling.BootJar

buildscript {
    repositories {
        mavenCentral()
        jcenter()
    }
}

repositories {
    mavenCentral()
    jcenter()
}

plugins {
    val kotlinVersion = "1.3.72"
    val palantirDockerVersion = "0.25.0"

    eclipse
    jacoco
    kotlin("jvm") version kotlinVersion
    kotlin("plugin.spring") version kotlinVersion
    id("org.springframework.boot") version "2.3.3.RELEASE"
    id("io.spring.dependency-management") version "1.0.10.RELEASE"
    id("com.palantir.docker") version palantirDockerVersion
    id("com.palantir.docker-run") version palantirDockerVersion
    id("io.gitlab.arturbosch.detekt") version "1.11.0"
}

group = "com.merkenlabs"
version = "0.1.0-SNAPSHOT"

tasks {
    "test"(Test::class) {
        useJUnitPlatform()
    }
    "bootJar"(BootJar::class) {
        version = ""
    }
    withType<Wrapper> {
        gradleVersion = "6.6"
    }

    withType<KotlinCompile> {
        kotlinOptions {
            jvmTarget = "1.8"
            freeCompilerArgs = listOf("-Xjsr305=strict")
        }
    }
}

repositories {
    mavenCentral()
}

dependencyManagement {
    imports {
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:Hoxton.SR7")
    }
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-thymeleaf")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.boot:spring-boot-starter-data-mongodb-reactive")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-oauth2-client")

    implementation("org.springframework.cloud:spring-cloud-starter-sleuth")
    implementation("org.springframework.security:spring-security-oauth2-resource-server")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    implementation("com.google.api-client:google-api-client")
    implementation("com.google.http-client:google-http-client-apache-v2")

    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))
    implementation("io.github.microutils:kotlin-logging:1.8.3")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")

    testImplementation("de.flapdoodle.embed:de.flapdoodle.embed.mongo")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
    testImplementation("io.projectreactor:reactor-test")
    testImplementation("org.springframework.security:spring-security-test")

    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
}

eclipse {
    project {
        natures("org.jetbrains.kotlin.core.kotlinNature")
    }
}

detekt {
    autoCorrect = true
    failFast = false

    config = files("detekt-config.yml")
    baseline = file("detekt-baseline.xml")
}

docker {
    dependsOn(tasks["bootJar"])
    name = "voy-api:$version"
    files(tasks["bootJar"].outputs)
    buildArgs(mutableMapOf("JAR_FILE" to "api.jar"))
}

dockerRun {
    name = "voy-api:latest"
    val localDirToMount = "/tmp/voy-api-volume"
    if (File(localDirToMount).exists()) {
        volumes(mutableMapOf(localDirToMount to "/tmp"))
    }
    ports("8080:8080")
    image = "api:$version"
    env(mutableMapOf("SPRING_PROFILES_ACTIVE" to "local-docker"))
    network = "voy"
    network = "mlabs"
}