FROM openjdk:8-jdk-alpine
MAINTAINER merkenlabs

ARG JAR_FILE
ENV JAR_FILE ${JAR_FILE}
VOLUME /tmp
ADD ${JAR_FILE} app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
EXPOSE 8080
